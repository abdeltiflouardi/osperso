<?php

//use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';

// Use APC for autoloading to improve performance.
// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.
/*
$loader = new ApcClassLoader('sf2', $loader);
$loader->register(true);
*/

require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
$kernel = new AppCache($kernel);
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();

if (0 != $request->query->get('X-PURGE', 0)) {
    $request->query->remove('X-PURGE');
    $qs = preg_replace('/([&]?)X-PURGE=1/', '', $request->server->get('QUERY_STRING'));
    $request->server->set('QUERY_STRING', $qs);

    $request->setMethod('PURGE');
}

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
