<?php

namespace OS\BlogBundle\EventListener;

use AntiMattr\GoogleBundle\Maps\StaticMap;
use AntiMattr\GoogleBundle\Maps\Marker;
use AntiMattr\GoogleBundle\MapsManager;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class MapsListener
{
    private $mapsManager;

    public function __construct(MapsManager $mapsManager)
    {
        $this->mapsManager = $mapsManager;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $map = new StaticMap();
        $map->setId("OS");
        $map->setSize("400x400");
        $map->setSensor(true);

        $marker = new Marker();
        $marker->setLatitude(33.585237);
        $marker->setLongitude(-7.577527);

        $map->addMarker($marker);

        $this->mapsManager->addMap($map);
    }
}