<?php

namespace OS\BlogBundle\Controller;

use OS\CommonBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{

    /**
     * @Route("/", name="_home")
     */
    public function indexAction()
    {
        $frontPost = $this->getPostEntityManager()->getQuestionsQuery(['p.front = 1'])->setMaxResults(1)->getOneOrNullResult();
        $this->set('frontPost', $frontPost);

        $frontPosts = $this->getPostEntityManager()->getQuestionsQuery(['p.front = 2'], array('p.createdAt' => 'DESC'))->setMaxResults(2)->getResult();
        $this->set('frontPosts', $frontPosts);

        $lastModified = $frontPost ? $frontPost->getCreatedAt() : new \DateTime($this->getParameter('version'));

        return $this->renderResponseCache(null, $lastModified);
    }
}
