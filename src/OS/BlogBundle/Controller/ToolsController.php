<?php

namespace OS\BlogBundle\Controller;

use OS\ToolsBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use OS\BlogBundle\PasswordGenerator\PasswordGeneratorRequest as PGRequest;
use OS\BlogBundle\PasswordGenerator\PasswordGeneratorType as PGForm;

class ToolsController extends Controller
{

    /**
     * @Route("/outils/generation-de-mot-de-passe/", name="_tools_password_generator")
     */
    public function passwordGeneratorAction(Request $request)
    {
        $pgReq = new PGRequest();
        $form = $this->createForm(new PGForm(), $pgReq);
        if ($request->query->get('password_generator')) {
            $form->bind($request);

            $this->flash(sprintf('Le mot de passe a été généré:<br /> %s', htmlentities($pgReq->generate())));
        }

        $this->set('form', $form->createView());

        return $this->renderResponse();
    }

    /**
     * @Route("/outils/iban/", name="_tools_iban")
     */
    public function ibanAction(Request $request)
    {
        return $this->renderResponse();
    }

}
