<?php

namespace OS\BlogBundle\Controller;

use OS\ToolsBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AboutController extends Controller
{

    /**
     * @Route("/about", name="_about"))
     */
    public function indexAction()
    {
        return $this->renderResponseCache();
    }
}
