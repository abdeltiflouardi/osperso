<?php

namespace OS\BlogBundle\Controller;

use OS\ToolsBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ContactController extends Controller
{

    /**
     * @Route("/contact-me/", name="_contact")
     */
    public function indexAction()
    {
        

        return $this->renderResponseCache();
    }
}
