<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace OS\BlogBundle\PasswordGenerator;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of PasswordGeneratorType
 *
 * @author ouardisoft
 */
class PasswordGeneratorType extends AbstractType
{
    /**
     * 
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('longeur')
                ->add('minuscule', 'checkbox')
                ->add('majuscule', 'checkbox')
                ->add('numeric', 'checkbox')
                ->add('symbol', 'checkbox');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('required' => false));
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'password_generator';
    }
}
