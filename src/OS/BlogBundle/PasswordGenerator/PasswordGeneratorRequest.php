<?php

namespace OS\BlogBundle\PasswordGenerator;

class PasswordGeneratorRequest
{
    private $longeur;
    private $minuscule;
    private $majuscule;
    private $numeric;
    private $symbol;

    public function __construct()
    {
        $this->longeur = 80;
        $this->minuscule  = true;
        $this->majuscule = true;
        $this->numeric = true;
        $this->symbol = true;
    }

    public function getLongeur()
    {
        return $this->longeur ?: 40;
    }

    public function getMinuscule()
    {
        return $this->minuscule;
    }

    public function getMajuscule()
    {
        return $this->majuscule;
    }

    public function getNumeric()
    {
        return $this->numeric;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setLongeur($longeur)
    {
        $this->longeur = $longeur;
        return $this;
    }

    public function setMinuscule($minuscule)
    {
        $this->minuscule = $minuscule;
        return $this;
    }

    public function setMajuscule($majuscule)
    {
        $this->majuscule = $majuscule;
        return $this;
    }

    public function setNumeric($numeric)
    {
        $this->numeric = $numeric;
        return $this;
    }

    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }

    public function generate()
    {
        $len = $this->getLongeur();
        $avg = 0;

        foreach (array('getMinuscule', 'getMajuscule', 'getNumeric', 'getSymbol') as $method) {
            if (call_user_func(array($this, $method))) {
                $avg++;
            }
        }

        if ($len < $avg) {
            $len = $avg;
        }

        $chars = '';
        if ($avg > 0) {
            $lenByCategory = ceil($len / $avg);
        } else {
            $chars = implode('', array_map('chr', range(33, 126)));
        }

        if ($this->getMinuscule()) {
            $mins = array();
            do {
                $mins = array_merge($mins, range('a', 'z'));
            } while (count($mins) < ($len * 4));

            shuffle($mins);

            $chars .= substr(implode('', $mins), 0, $lenByCategory);
        }

        if ($this->getMajuscule()) {
            $majs = array();
            do {
                $majs = array_merge($majs, range('A', 'Z'));
            } while (count($majs) < ($len * 4));

            shuffle($majs);

            $chars .= substr(implode('', $majs), 0, $lenByCategory);
        }

        if ($this->getNumeric()) {
            $num = array();
            do {
                $num = array_merge($num, range(0, 9));
            } while (count($num) < ($len * 4));

            shuffle($num);

            $chars .= substr(implode('', $num), 0, $lenByCategory);
        }

        if ($this->getSymbol()) {
            $symb = array();
            do {
                $symb = array_merge($symb, str_split('!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~'));
            } while (count($symb) < ($len * 4));

            shuffle($symb);

            $chars .= substr(implode('', $symb), 0, $lenByCategory);
        }

        $password = str_split($chars);

        shuffle($password);

        return substr(implode('', $password), 0, $len);
    }
}
