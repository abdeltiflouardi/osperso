<?php

namespace OS\CommonBundle\Controller;

use OS\ToolsBundle\Controller\BaseController as Controller;
use OS\BlogBundle\EntityManager\PostManager as PostEntityManager;
use OS\BlogBundle\FormManager\PostManager as PostFormManager;

/**
 * Description of BaseController
 *
 * @author ouardisoft
 */
class BaseController extends Controller
{
    /**
     * @return PostEntityManager
     */
    public function getPostEntityManager()
    {
        return $this->container->get('os_post.entity.manager');
    }

    /**
     * @return PostFormManager
     */
    public function getPostFormManager()
    {
        return $this->container->get('os_post.form.manager');
    }
}
