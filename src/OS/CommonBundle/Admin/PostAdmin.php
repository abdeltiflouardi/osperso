<?php

namespace OS\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PostAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('postType')
            ->add('title')
            ->add('content')
            ->add('enabled', null, array('required' => false))
            ->add('front', 'choice', array('choices' => ['Normal', 'À la une', 'À la une (2)']))
            ->add('tag', 'tag');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('front')
            ->add('tag');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('title')
            ->add('front')
            ->add('enabled');
    }
}
